﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using guitech.Models;

namespace guitech.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        /* Desenvolvido em 23/01/2021 por <guilherme.v.moreira> - Início */
        public DbSet<Categoria> Categorias {get; set;}
        public DbSet<Fornecedor> Fornecedores {get; set;}
        public DbSet<Produto> Produtos {get; set;}
        public DbSet<Promocao> Promocoes {get; set;}
        public DbSet<Estoque> Estoques {get; set;}
        public DbSet<Saida> Saidas {get; set;}
        public DbSet<Venda> Vendas {get; set;}
        /* Desenvolvido em 23/01/2021 por <guilherme.v.moreira> - FIM */
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
